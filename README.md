<p  align="center">
	<a  href="https://fossnssorg">#FOSSNSS</a>
</p>


# FOSS Cell NSSCE 

 
This is the home page repo for the Free and Open Source Cell of NSS College of Engineering, Palakkad.


## 🚀 Quick start

  

1.  **Install NodeJs and NPM.**

  

We recommend using NodeJs version 12(LTS). To manage different versions of NodeJS installations, you may use `nvm` script for `bash` or your favourite shell.
Follow [this](https://github.com/nvm-sh/nvm) official guide to add `nvm` to your shell

  

```shell

# install NodeJs v12 and use it in nvm
nvm install 12
nvm use 12

```


2.  **Install GatsbyJS.**

Now you need to install GatsbyJS. Which is used to generate our static site.
Follow [this official guide](https://www.gatsbyjs.org/tutorial/part-zero/#using-the-gatsby-cli) to install GatsbyJS onto your machine.
  

```shell
npm install -g gatsby-cli
```

  

3.  **Clone this repo.**

  

Now you need to clone this repo using `git` to your local machine to develop or to test.

  

```shell

#if you have not added ssh keys to your gitlab account(😥)
git clone https://gitlab.com/fossnss/website.git fossnss

#else, if you have ssh added your ssh keys 🤩
git clone git@gitlab.com:fossnss/website.git fossnss

cd fossnss/

gatsby develop

```

  

1.  **Open the source code and start editing!**

  

Your site is now running at `http://localhost:8000`!

  

_Note: You'll also see a second link: _`http://localhost:8000/___graphql`_. This is a tool you can use to experiment with querying your data. Learn more about using this tool in the [Gatsby tutorial](https://www.gatsbyjs.org/tutorial/part-five/#introducing-graphiql)._

  

Open the `fossnss`(or whatever you have named) directory in your code editor of choice and edit `src/pages/index.js`. Save your changes and the browser will update in real time!

Before helping us by pushing changes, make sure to not to break things 😜 

## 🧐 What's inside?

  We still need to structure our project a little bit to showcase what is what....
  A little help, maybe 😊
